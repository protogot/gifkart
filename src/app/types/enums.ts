export enum API_URLS {
  TRENDING_GIFS = "gifs/trending",
  TRENDING_STICKERS = "stickers/trending",
  SEARCH_GIFS = "gifs/search",
  SEARCH_STICKERS = "stickers/search",
}

export enum API_METHOD {
  GET = "get",
  POST = "post",
  PUT = "put",
  DELETE = "delete"
}