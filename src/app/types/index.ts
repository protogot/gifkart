export type NavItemType = {
  label: string;
  id: 2 | 3 | 4 | 5;
}

export type GIFListPropsType = {
  
}

export type AxiosPropsType = {

}

export type GIFRawType = {
  type: string,
  id: string,
  url: string,
  slug: string,
  bitly_gif_url: string,
  bitly_url: string,
  embed_url: string,
  username: string,
  source: string,
  title: string,
  rating: string,
  content_url: string,
  source_tld: string,
  source_post_url: string,
  is_sticker: number,
  import_datetime: string,
  trending_datetime: string,
  images: Record<string, {
    frames: string
    hash: string
    height: string
    mp4: string
    mp4_size: string
    size: string
    url: string
    webp: string
    webp_size: string
    width: string
  }>,
  user: Record<string, any>,
  analytics_response_payload: string
  analytics: Record<string, any>,
  handleGifItemClick: (event: any) => void;
}