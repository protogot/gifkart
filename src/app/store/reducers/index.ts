import { combineReducers } from "redux";

import GIFListReducer from "./GIFListReducer";
import navigationReducer from "./navigationReducer";
import searchReducer from "./searchReducer";
import themeReducer from "./themeReducer";

const rootReducer = combineReducers({
  navigationState: navigationReducer,
  gifListState: GIFListReducer,
  searchState: searchReducer,
  themeState: themeReducer
});

export default rootReducer;