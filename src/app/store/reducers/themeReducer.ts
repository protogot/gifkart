import { ThemeActionEnums, ThemeActionPayloadType, ThemeActionType } from "./types";

export default function themeReducer(state: ThemeActionPayloadType["payload"] = {
  activeTheme: "dark"
}, action: ThemeActionType){
  switch(action.type){
    case ThemeActionEnums.updateCurrenTheme:
      state = {
        activeTheme: action.payload.activeTheme
      }
      return state;
    default:
      return state;
  }
}