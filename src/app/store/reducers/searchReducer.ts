import {
  SearchActionEnums,
  SearchActionPayloadType,
  SearchActionType,
} from "./types";

export default function searchReducer(
  state: SearchActionPayloadType["payload"] = {},
  action: SearchActionType
) {
  switch (action.type) {
    case SearchActionEnums.updateSearchConfig:
      state = {
        ...state,
        searchConfig: {
          ...state.searchConfig,
          ...action.payload
        }
      };
      return state;
    default:
      return state;
  }
}
