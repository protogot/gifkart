import { GIFRawType } from "app/types";
import { GIFListActionType, GIFListEnums } from "./types";

export default function GIFListReducer(
  state: {
    gifs?: Array<GIFRawType>,
    stickers?: Array<GIFRawType>
  } = {
    gifs: [],
    stickers: []
  },
  action: GIFListActionType
) {
  switch (action.type) {
    case GIFListEnums.updateGIFList:
      state = {
        ...state,
        gifs: action.payload.gifs
      };
      return state;
    case GIFListEnums.updateStickersList:
      state = {
        ...state,
        stickers: action.payload.stickers
      };
      return state;
    default:
      return state;
  }
}
