import { NavigationActionEnums, NavigationActionType, NavigationReducerState } from "./types";

export default function navigationReducer(
  state: NavigationReducerState = {
    activeTabId: 2
  },
  action: NavigationActionType
) {
  switch (action.type) {
    case NavigationActionEnums.activeTab:
      state = { ...state, activeTabId: action.payload.activeTabId };
      return state;
    default:
      return state;
  }
}
