import { GIFRawType } from "app/types";

export enum NavigationActionEnums {
  activeTab = "ACTIVE_TAB"
}

export enum GIFListEnums {
  updateGIFList = "UPDATE_GIF_LIST",
  updateStickersList = "UPDATE_STICKERS"
}

export enum SearchActionEnums {
  updateSearchConfig = "UPDATE_SEARCH_CONFIG"
}

export enum ThemeActionEnums {
  updateCurrenTheme = "UPDATE_CURRENT_THEME"
}

export interface NavigationActionType extends NavigationActionPayloadType {
  type: NavigationActionEnums,
}

export type NavigationActionPayloadType = {
  payload: {
    activeTabId: number
  }
}

export interface NavigationReducerState {
  activeTabId?: number;
}

export interface GIFListActionType extends GIFListActionPayloadType {
  type: GIFListEnums
}

export interface GIFListActionPayloadType {
  payload: {
    gifs?: Array<GIFRawType> | [],
    stickers?: Array<GIFRawType>| []
  }
}

export interface SearchActionType extends SearchActionPayloadType {
  type: SearchActionEnums
}

export interface SearchActionPayloadType {
  payload: {
    searchConfig?: {
      userInput?: string,
      isLoading?: boolean
    }
  }
}

export interface ThemeActionType extends ThemeActionPayloadType {
  type: ThemeActionEnums
}

export interface ThemeActionPayloadType {
  payload: {
    activeTheme: "light" | "dark";
  }
}