import {
  NavigationActionEnums,
  NavigationActionPayloadType,
  NavigationActionType,
} from "../reducers/types";

export function updateActiveNavigationTab(
  data: NavigationActionPayloadType["payload"]
): NavigationActionType {
  return {
    type: NavigationActionEnums.activeTab,
    payload: data,
  };
}
