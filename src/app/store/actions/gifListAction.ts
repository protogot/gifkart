import {
  GIFListActionPayloadType,
  GIFListActionType,
  GIFListEnums,
} from "../reducers/types";

export function updateGIFListAction(
  data: GIFListActionPayloadType["payload"]
): GIFListActionType {
  return {
    type: GIFListEnums.updateGIFList,
    payload: data,
  };
}

export function updateStickerListAction(
  data: GIFListActionPayloadType["payload"]
): GIFListActionType {
  return {
    type: GIFListEnums.updateStickersList,
    payload: data,
  };
}
