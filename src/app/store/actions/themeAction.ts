import { ThemeActionEnums, ThemeActionPayloadType } from "../reducers/types";

export function updateCurrentThemeAction(data: ThemeActionPayloadType["payload"]){
  return {
    type: ThemeActionEnums.updateCurrenTheme,
    payload: data
  }
}