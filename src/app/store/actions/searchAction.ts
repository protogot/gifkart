import { SearchActionEnums, SearchActionType } from "../reducers/types";

export function updateSearchConfigAction(data: SearchActionType["payload"]["searchConfig"]){
  return {
    type: SearchActionEnums.updateSearchConfig,
    payload: data
  }
}