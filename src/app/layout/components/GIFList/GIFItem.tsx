import { loadImage } from "app/core/cache/ImageResource";
import { GIFRawType } from "app/types";
import { GridContainer, GridItem } from "layout/common/Grid/GridContainer";
import React, { MouseEventHandler } from "react";
import { PauseCircleFilled, PlayCircleFilled } from "@material-ui/icons";

function SuspenseImage(props: React.ImgHTMLAttributes<HTMLImageElement>) {
  loadImage(props.src || "").read();
  return (
    <img
      src={props.src}
      alt={props.title}
      loading="lazy"
      onClick={props.onClick}
    />
  );
}

function GIFItem(gif: GIFRawType) {
  enum GIFState {
    paused,
    play,
  }
  const [currentGIFState, updateGIFState] = React.useState<GIFState>(
    GIFState.play
  );
  const [isHoverActive, updateIsHoverActive] = React.useState<boolean>(false);

  const handleGifItemClick = (event: any) => {
    switch (currentGIFState) {
      case GIFState.paused:
        updateGIFState(GIFState.play);
        break;
      case GIFState.play:
        updateGIFState(GIFState.paused);
        break;
    }
  };
  const commonProps: React.ImgHTMLAttributes<HTMLImageElement> = {
    title: gif.title
  };

  const toggleHoverInteration = (event: any) => {
    switch (event.type) {
      case "mouseleave":
        isHoverActive && updateIsHoverActive(false);
        break;
      case "mouseenter":
        !isHoverActive && updateIsHoverActive(true);
        break;
    }
  };

  function GIFItemOverlay(gif: GIFRawType) {
    const { handleGifItemClick, user } = gif;
    return (
      <GridContainer className="gif-item-overlay">
        {currentGIFState === GIFState.play ? (
          <PauseCircleFilled fontSize="large" className="gif-overlay-action" onClick={handleGifItemClick} />
        ) : (
          <PlayCircleFilled fontSize="large" className="gif-overlay-action" onClick={handleGifItemClick} />
        )}
        {user && <a className="user-container" href={user.profile_url} target="_blank"  title="View Profile">
          <img src={user.avatar_url} className="avatar-logo"/>
          <GridItem className="avatar-label">
            <GridItem className="title">{user.username}</GridItem>
            <GridItem className="sub-title">{user.description}</GridItem>
          </GridItem>
        </a>}
      </GridContainer>
    );
  }

  return (
    <GridItem
      key={gif.id}
      className="gif-item-container"
      style={{
        height: gif.images.fixed_height.height + "px",
        width: gif.images.fixed_height.width + "px"
      }}
      onMouseEnter={toggleHoverInteration}
      onMouseLeave={toggleHoverInteration}
    >
      {currentGIFState === GIFState.play && (
        <SuspenseImage src={gif.images.fixed_height.url} {...commonProps} />
      )}

      <GridItem
        style={{
          display: currentGIFState === GIFState.paused ? "unset" : "none",
        }}
      >
        <SuspenseImage
          src={gif.images.fixed_height_still.url}
          {...commonProps}
        />
      </GridItem>
      {(isHoverActive || currentGIFState === GIFState.paused) && (
        <GIFItemOverlay {...gif} handleGifItemClick={handleGifItemClick} />
      )}
    </GridItem>
  );
}

const arePropsEqual = (prevProps: GIFRawType, nextProps: GIFRawType) => {
  /**
   * TODO : Implement Deep Cloning
   */
  if (JSON.stringify(prevProps) !== JSON.stringify(nextProps)) {
    return false;
  }

  return true;
};
const GIFItemMemo = React.memo(GIFItem, arePropsEqual);
export default GIFItemMemo;
