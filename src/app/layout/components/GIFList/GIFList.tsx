import { usePrevious } from "app/core/custom-hooks";
import { END_POINTS } from "app/core/endpoints";
import {
  SEARCH_GIFS_PARAMS_TYPE,
  TRENDING_GIFS_PARAMS_TYPE,
  TRENDING_STICKERS_PARAMS_TYPE,
} from "app/core/endpoints/types";
import { APILayer } from "app/core/network/api";
import {
  updateGIFListAction,
  updateStickerListAction,
} from "app/store/actions/gifListAction";
import {
  GIFListActionPayloadType,
  NavigationActionPayloadType,
  SearchActionPayloadType,
} from "app/store/reducers/types";
import { GIFListPropsType, GIFRawType } from "app/types";
import { GridContainer, GridItem } from "layout/common/Grid/GridContainer";
import Loader from "layout/common/Loader/Loader";
import React from "react";
import { useCallback } from "react";
import { useEffect } from "react";
import { connect, ConnectedProps } from "react-redux";
const GIFItem = React.lazy(() => import("./GIFItem"));

const randomColors = ["#25AF6A", "#25A0AF", "#AF3E25", "#519BDF", "#DF5198"];

const mapStateToProps = (state: Record<string, any>) => state;
const mapDispatch = (dispatch: any) => {
  return {
    updateGifList: (props: GIFListActionPayloadType["payload"]) =>
      dispatch(updateGIFListAction(props)),
    updateStickerList: (props: GIFListActionPayloadType["payload"]) =>
      dispatch(updateStickerListAction(props)),
  };
};

const connector = connect(mapStateToProps, mapDispatch);
type PropsFromRedux = ConnectedProps<typeof connector>;

interface Props extends GIFListPropsType, PropsFromRedux {
  navigationState?: NavigationActionPayloadType["payload"];
  gifListState?: GIFListActionPayloadType["payload"];
  searchState?: SearchActionPayloadType["payload"];
}

function GIFList(props: Props) {
  const {
    gifListState,
    updateGifList,
    navigationState,
    updateStickerList,
    searchState,
  } = props;
  const [paginationOffset, updatePaginationOffset] = React.useState<number>(0);
  const [isLoading, updateIsLoading] = React.useState<boolean>(false);
  const prevNavigationState: NavigationActionPayloadType["payload"] =
    usePrevious(navigationState) || {
      activeTabId: 2,
    };

  const handlePageScroll = useCallback((event: any) => {
    const {
      target: { scrollTop, scrollHeight, clientHeight },
    } = event;
    const scrollSpace = scrollTop + clientHeight;

    if (Math.floor(scrollSpace) === Math.floor(scrollHeight)) {
      updatePaginationOffset((prevPaginationOffset) => {
        return prevPaginationOffset + 20;
      });
    }
  }, []);

  const successCallback = useCallback(
    (response: any) => {
      const GIFS: Array<GIFRawType> = response.data;
      if (navigationState?.activeTabId === 2) {
        updateGifList({
          gifs: [...(gifListState?.gifs || []), ...GIFS],
        });
      } else if (navigationState?.activeTabId === 3) {
        updateStickerList({
          stickers: [...(gifListState?.stickers || []), ...GIFS],
        });
      }
      updateIsLoading(false);
    },
    [gifListState, navigationState]
  );

  const errorCallback = useCallback(
    (error: any) => {
      updateIsLoading(false);
      /**
       * TODO : Handle error callback using a global fn
       */
    },
    [gifListState, navigationState]
  );

  const fetchGIFs = useCallback(() => {
    const apiParams: TRENDING_GIFS_PARAMS_TYPE | SEARCH_GIFS_PARAMS_TYPE = {
      q: searchState?.searchConfig?.userInput,
      limit: 20,
      offset: paginationOffset,
    };
    const URL = searchState?.searchConfig?.userInput
      ? END_POINTS.urls.SEARCH_GIFS
      : END_POINTS.urls.TRENDING_GIFS;
    updateIsLoading(true);
    APILayer.EXECUTE({
      apiConfig: {
        URL: URL,
        METHOD: END_POINTS.methods.GET,
        PARAMS: apiParams,
      },
      successCallback,
      errorCallback,
    });
  }, [paginationOffset, navigationState]);

  const fetchStickers = useCallback(() => {
    const apiParams: TRENDING_STICKERS_PARAMS_TYPE | SEARCH_GIFS_PARAMS_TYPE = {
      q: searchState?.searchConfig?.userInput,
      limit: 20,
      offset: paginationOffset,
    };
    const URL = searchState?.searchConfig?.userInput
      ? END_POINTS.urls.SEARCH_STICKERS
      : END_POINTS.urls.TRENDING_STICKERS;
    updateIsLoading(true);
    APILayer.EXECUTE({
      apiConfig: {
        URL: URL,
        METHOD: END_POINTS.methods.GET,
        PARAMS: apiParams,
      },
      successCallback,
      errorCallback,
    });
  }, [paginationOffset, navigationState]);

  useEffect(() => {
    const scrollElement = document.querySelector(".page-body");
    scrollElement?.addEventListener("scroll", handlePageScroll);
    return () => {
      scrollElement?.removeEventListener("scroll", handlePageScroll);
    };
  }, [handlePageScroll]);

  useEffect(() => {
    if (navigationState?.activeTabId === 2) {
      fetchGIFs();
    } else if (navigationState?.activeTabId === 3) {
      fetchStickers();
    }
  }, [paginationOffset]);

  useEffect(() => {
    const currentActiveTabId = navigationState?.activeTabId;

    if (prevNavigationState.activeTabId !== currentActiveTabId) {
      if (currentActiveTabId === 2 && !gifListState?.gifs?.length) {
        fetchGIFs();
      } else if (currentActiveTabId === 3 && !gifListState?.stickers?.length) {
        fetchStickers();
      }
    }
  }, [navigationState]);

  const fallbackComponent = (gif: GIFRawType) => {
    const currentRandomBG =
      randomColors[Math.floor(Math.random() * randomColors.length)];
    return (
      <GridItem
        style={{
          height: gif.images.fixed_height.height + "px",
          width: gif.images.fixed_height.width + "px",
          backgroundColor: currentRandomBG,
        }}
        className="gif-item-fallback-container"
      />
    );
  };

  const gifsList: Array<GIFRawType> =
    navigationState?.activeTabId === 2
      ? gifListState?.gifs || []
      : gifListState?.stickers || [];

  const isSearchLoading = searchState?.searchConfig?.isLoading;

  return (
    <>
      {!isSearchLoading && (
        <GridContainer className="gif-items-container">
          {gifsList.length
            ? gifsList.map((gif: GIFRawType) => (
                <React.Suspense
                  key={gif.id + new Date() + Math.random()}
                  fallback={fallbackComponent(gif)}
                >
                  <GIFItem key={gif.id} {...gif} />
                </React.Suspense>
              ))
            : !isSearchLoading &&
              !isLoading && (
                <GridContainer className="no-results-found-container">
                  <GridItem xs={8}>
                    <GridItem xs={12} className="no-results-found-label">
                      No result found
                    </GridItem>
                    <GridItem xs={12} className="no-results-found-sub-label">
                      Please check if there is any spelling mistake and retry.
                    </GridItem>
                  </GridItem>
                </GridContainer>
              )}
        </GridContainer>
      )}
      {(isLoading || isSearchLoading) && <Loader />}
    </>
  );
}

export default connector(GIFList);
