import React from "react";
import Body from "layout/common/Body/Body";
import { GridContainer } from "layout/common/Grid/GridContainer";
import Navigation from "layout/common/Navigation/Navigation";
import GIFList from "layout/components/GIFList/GIFList";

import "./Landing.scss";
import { RouteComponentProps, RouteProps } from "react-router-dom";
import { connect, ConnectedProps } from "react-redux";
import { NavigationActionPayloadType, SearchActionPayloadType, ThemeActionPayloadType } from "app/store/reducers/types";

const mapStateToProps = (state: any) => state;
const mapDispatch = (dispatch: any) => {
  return {

  }
}

const connector = connect(mapStateToProps, mapDispatch);
type PropsFromRedux = ConnectedProps<typeof connector>;

type Props = RouteComponentProps<any> & RouteProps & PropsFromRedux & {
  route?: any;
  searchState?: SearchActionPayloadType["payload"],
  themeState?: ThemeActionPayloadType["payload"],
  navigationState?: NavigationActionPayloadType["payload"]
};

function Landing(props: Props){
  const { themeState: { activeTheme = "dark" } = {}, searchState : { searchConfig } = {}, navigationState : {activeTabId} = {}  } = props;
  const activeTabLabel = activeTabId === 2 ? "GIFs" : "Stickers";
  let Heading = `Trending ${activeTabLabel} from Renowned Authors`;
  if(searchConfig?.userInput){
    switch(searchConfig.isLoading){
      case true:
        Heading = `Searching ${activeTabLabel} ...`;
      default:
        Heading = `Search results for "${searchConfig.userInput}" ${activeTabLabel} `;
    }
  }
  return (
    <GridContainer className="page-landing-container" data-theme={activeTheme}>
      <Navigation/>
      <Body>
        <h1 className="section-main-heading">
          {Heading}
        </h1>
        <GIFList/>
      </Body>
    </GridContainer>
  )
}

export default connector(Landing);