import { END_POINTS } from "app/core/endpoints";
import { SEARCH_GIFS_PARAMS_TYPE } from "app/core/endpoints/types";
import { APILayer } from "app/core/network/api";
import {
  updateGIFListAction,
  updateStickerListAction,
} from "app/store/actions/gifListAction";
import { updateSearchConfigAction } from "app/store/actions/searchAction";
import {
  GIFListActionPayloadType,
  NavigationActionPayloadType,
  SearchActionPayloadType,
} from "app/store/reducers/types";
import { GIFRawType } from "app/types";
import React, { useCallback, useEffect } from "react";
import { connect, ConnectedProps } from "react-redux";
import { GridContainer, GridItem } from "../Grid/GridContainer";
import "./Search.scss";

const mapStateToProps = (state: any) => state;
const mapDispatch = (dispatch: any) => {
  return {
    updateGifList: (props: GIFListActionPayloadType["payload"]) =>
      dispatch(updateGIFListAction(props)),
    updateStickerList: (props: GIFListActionPayloadType["payload"]) =>
      dispatch(updateStickerListAction(props)),
    updateSearchConfig: (
      props: SearchActionPayloadType["payload"]["searchConfig"]
    ) => dispatch(updateSearchConfigAction(props)),
  };
};

const connector = connect(mapStateToProps, mapDispatch);
type PropsFromRedux = ConnectedProps<typeof connector>;

type Props = PropsFromRedux & {
  gifListState?: GIFListActionPayloadType["payload"];
  navigationState?: NavigationActionPayloadType["payload"];
  searchState?: SearchActionPayloadType["payload"];
  updateGifList?: (props: GIFListActionPayloadType["payload"]) => void;
  updateStickerList?: (props: GIFListActionPayloadType["payload"]) => void;
  updateSearchConfig?: (
    props: SearchActionPayloadType["payload"]["searchConfig"]
  ) => void;
};

const PAGINATION_OFFSET = 20;

function Search(props: Props) {
  const {
    updateGifList,
    gifListState,
    navigationState,
    updateStickerList,
    updateSearchConfig,
  } = props;
  const [userInput, updateUserInput] = React.useState<string>();
  const [debounceInputFn, updateDebounceInputFn] = React.useState<any>();

  const isGIFTabActive = navigationState?.activeTabId === 2;

  const handleInputChange = useCallback(
    (event) => {
      clearInterval(debounceInputFn);
      const targetValue = event.target.value;
      updateUserInput(targetValue);
      updateDebounceInputFn(
        setTimeout(() => {
          handleInputChangeFinally(targetValue);
        }, 1000)
      );
      return () => {
        clearTimeout(debounceInputFn);
      };
    },
    [debounceInputFn]
  );

  const handleInputChangeFinally = useCallback(
    (targetValue: string) => {
      updateSearchConfig &&
        updateSearchConfig({
          userInput: targetValue,
        });
      if (isGIFTabActive) {
        fetchGIFsWithQuery(targetValue);
      } else {
        fetchStickersWithQuery(targetValue);
      }
    },
    [debounceInputFn]
  );

  const successCallback = useCallback(
    (response: any) => {
      const GIFS: Array<GIFRawType> = response.data;
      if (isGIFTabActive) {
        updateGifList &&
          updateGifList({
            gifs: GIFS,
          });
      } else {
        updateStickerList &&
          updateStickerList({
            stickers: GIFS,
          });
      }
      updateSearchConfig &&
        updateSearchConfig({
          isLoading: false,
        });
    },
    [gifListState, navigationState]
  );

  const errorCallback = useCallback(
    (error: any) => {
      updateSearchConfig &&
        updateSearchConfig({
          isLoading: false,
        });
      /**
       * TODO : Handle error callback using a global fn
       */
    },
    [gifListState, navigationState]
  );

  const fetchGIFsWithQuery = useCallback(
    (userInput) => {
      const apiParams: SEARCH_GIFS_PARAMS_TYPE = {
        q: userInput,
        limit: 20,
        offset: PAGINATION_OFFSET,
      };
      updateSearchConfig &&
        updateSearchConfig({
          isLoading: true,
        });
      APILayer.EXECUTE({
        apiConfig: {
          URL: userInput
            ? END_POINTS.urls.SEARCH_GIFS
            : END_POINTS.urls.TRENDING_GIFS,
          METHOD: END_POINTS.methods.GET,
          PARAMS: apiParams,
        },
        successCallback,
        errorCallback,
      });
    },
    [navigationState]
  );

  const fetchStickersWithQuery = useCallback(
    (userInput) => {
      const apiParams: SEARCH_GIFS_PARAMS_TYPE = {
        q: userInput,
        limit: 20,
        offset: PAGINATION_OFFSET,
      };
      updateSearchConfig &&
        updateSearchConfig({
          isLoading: true,
        });
      APILayer.EXECUTE({
        apiConfig: {
          URL: userInput
            ? END_POINTS.urls.SEARCH_STICKERS
            : END_POINTS.urls.TRENDING_STICKERS,
          METHOD: END_POINTS.methods.GET,
          PARAMS: apiParams,
        },
        successCallback,
        errorCallback,
      });
    },
    [navigationState]
  );

  useEffect(() => {
    if (props.searchState?.searchConfig?.userInput !== userInput) {
      updateUserInput("");
    }
  }, [props.searchState?.searchConfig?.userInput]);

  return (
    <GridContainer className="gifkart-search">
      <GridItem xs={12}>
        <input
          placeholder={"Search GIFs across multiple categories"}
          type="text"
          onChange={handleInputChange}
          value={userInput} 
        />
      </GridItem>
    </GridContainer>
  );
}

export default connector(Search);
