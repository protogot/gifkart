import React, { ReactElement } from "react";
import KButton from "../Button/Button";
import { GridContainer, GridItem } from "../Grid/GridContainer";
import "./Navigation.scss";
import I18n from "./../../../local/I18n.json";
import { NavItemType } from "app/types";
import Search from "../Search/Search";
import { connect, ConnectedProps } from "react-redux";
import { updateActiveNavigationTab } from "app/store/actions/navigationAction";
import {
  NavigationActionPayloadType,
  SearchActionPayloadType,
  ThemeActionPayloadType,
} from "app/store/reducers/types";
import { updateSearchConfigAction } from "app/store/actions/searchAction";
import { updateCurrentThemeAction } from "app/store/actions/themeAction";
import { useCallback } from "react";

declare const window: any;

const mapDispatch = (dispatch: any) => {
  return {
    updateActiveTab: (props: NavigationActionPayloadType["payload"]) =>
      dispatch(updateActiveNavigationTab(props)),
    updateSearchConfig: (
      props: SearchActionPayloadType["payload"]["searchConfig"]
    ) => dispatch(updateSearchConfigAction(props)),
    updateCurrentTheme: (props: ThemeActionPayloadType["payload"]) =>
      dispatch(updateCurrentThemeAction(props)),
  };
};

const mapStateToProps = (state: Record<string, any>) => state;

const connector = connect(mapStateToProps, mapDispatch);
type PropsFromRedux = ConnectedProps<typeof connector>;

type Props = PropsFromRedux & {
  updateActiveTab: (props: NavigationActionPayloadType["payload"]) => void;
  navigationState?: NavigationActionPayloadType["payload"];
  updateSearchConfig?: (
    props: SearchActionPayloadType["payload"]["searchConfig"]
  ) => void;
  updateCurrentTheme?: (props: ThemeActionPayloadType["payload"]) => void;
  themeState?: ThemeActionPayloadType["payload"]
};

const navItemsList: Array<NavItemType> = [
  {
    label: I18n.GIFS,
    id: 2,
  },
  {
    label: I18n.STICKERS,
    id: 3,
  },
];

function Navigation(props: Props) {
  const NavItem = useCallback(({
    label,
    id,
    isActive,
    width,
  }: {
    label: string | ReactElement;
    id: number;
    isActive: boolean;
    width?: number;
  }) => {
    const dataName =
      typeof label === "string"
        ? label.split(" ").join("-").toLocaleLowerCase()
        : "--";
    return (
      <GridItem
        xs={width || 3}
        className={`nav-items ${isActive && "active"}`}
        data-name={dataName}
        onClick={() => handleNavItem(id)}
      >
        {label || "--"}
      </GridItem>
    );
  }, []);

  const handleNavItem = useCallback((activeTabId: number) => {
    const scrollingElement = document.querySelector(".page-body");
    if(scrollingElement) scrollingElement.scrollTop = 0;
    
    props.updateActiveTab({
      activeTabId,
    });
    props.updateSearchConfig({
      userInput: undefined,
    });
  }, []);

  const handleSwitchTheme = useCallback((event: any) => {
    if(props.themeState?.activeTheme === "dark"){
      props.updateCurrentTheme({
        activeTheme: "light"
      });
    } else {
      props.updateCurrentTheme({
        activeTheme: "dark"
      });
    }
  }, [props.themeState?.activeTheme]);

  return (
    <GridContainer className="page-navigation">
      <GridItem className="logo-container" xs={6} sm={6} md={2} lg={2}>
        <img alt="gifkart-logo" src="public/gifkart.png" />
      </GridItem>
      <GridItem className="nav-links-container" md={4} lg={4}>
        <GridContainer className="nav-links-wrapper">
          {navItemsList.map(({ label, id }: NavItemType) => (
            <NavItem
              key={id}
              id={id}
              label={label}
              isActive={id === props.navigationState?.activeTabId}
            />
          ))}
        </GridContainer>
      </GridItem>
      <GridItem md={4} lg={4} className="gifkart-search-wrapper">
        <Search />
      </GridItem>
      <GridItem xs={6} sm={6} md={2} lg={2} className="actions">
        <KButton className="gifkart-signup" type="default" onClick={handleSwitchTheme}>
          Switch Theme
        </KButton>
      </GridItem>
    </GridContainer>
  );
}

export default connector(Navigation);
