import { Grid } from "@material-ui/core";
import React, { DOMAttributes } from "react";

interface ContainerProps extends DOMAttributes<any> {
  className?: string;
  id?: string;
  onClick?: any;
}
interface ContainerState {}

class GridContainer extends React.Component<ContainerProps, ContainerState> {
  constructor(props: ContainerProps) {
    super(props);
    this.state = {};
  }

  public render() {
    const { props } = this;
    return (
      <Grid container {...props}>
        {props.children}
      </Grid>
    );
  }
}

interface ItemProps extends DOMAttributes<any> {
  span?: number;
  className?: string;
  id?: string;
  xs?: any;
  sm?: any;
  md?: any;
  lg?: any;
  xl?: any;
  innerRef?: any;
  style?: Record<string, any>;
}
interface ItemState {}

class GridItem extends React.Component<ItemProps, ItemState> {
  constructor(props: ItemProps) {
    super(props);
    this.state = {};
  }

  public render() {
    const { props } = this;
    return (
      <Grid item {...props} innerRef={props.innerRef}>
        {props.children}
      </Grid>
    );
  }
}

export { GridContainer, GridItem };
