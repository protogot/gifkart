import React from "react";
import { GridContainer, GridItem } from "../Grid/GridContainer";

function Loader() {
  return (
    <>
      <GridContainer className="progress-bar-wrap-container">
        <GridContainer className="progress-bar-container">
          <GridItem className="progress-bar"></GridItem>
        </GridContainer>
      </GridContainer>
    </>
  );
}

const LoaderMemo = React.memo(Loader);
export default LoaderMemo;
