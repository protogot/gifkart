import React from "react";
import { Button, ButtonTypeMap } from "@material-ui/core";

type Props = {
  className?: string;
  type?: "primary" | "default" | "secondary";
  disabled?: boolean;
  onClick?: (event: any) => void
};

type State = {};

export default class KButton extends React.PureComponent<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {};
  }

  public render() {
    const { children, type = "default", disabled, className, onClick } = this.props;
    return (
      <Button variant="contained" color={type} disabled={disabled} className={className} onClick={onClick}>
        {children}
      </Button>
    );
  }
}
