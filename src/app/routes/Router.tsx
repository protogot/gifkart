import React from "react";
import AppContainer from "./../layout/common/AppContainer/AppContainer";

import { BrowserRouter, Switch, Route } from "react-router-dom";
import Routes from "./Routes.json";

import Landing from "layout/pages/Landing";

export default function Router() {
  return (
    <AppContainer>
      <BrowserRouter>
        <Switch>
          <Route path={Routes.LANDING_ROUTE} component={Landing} exact />
        </Switch>
      </BrowserRouter>
    </AppContainer>
  );
}
