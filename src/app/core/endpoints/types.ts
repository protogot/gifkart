export interface TRENDING_GIFS_PARAMS_TYPE {
  limit?: number;
  offset?: number;
}

export interface TRENDING_STICKERS_PARAMS_TYPE
  extends TRENDING_GIFS_PARAMS_TYPE {}

export interface SEARCH_GIFS_PARAMS_TYPE {
  limit?: number;
  offset?: number;
  q: string;
}
