import { ROOT_URL, API_KEY } from "app/core/endpoints";
import { AxiosPropsType } from "app/types";
import { API_METHOD } from "app/types/enums";
import axios, { AxiosResponse } from "axios";

type API_CONFIG_TYPE = {
  URL: string;
  METHOD: API_METHOD;
  PARAMS?: Record<string, any> | Array<Record<string, any>>;
};

type API_LAYER_PARAMS_TYPE = {
  apiConfig: API_CONFIG_TYPE;
  errorCallback: NW_CALLBACK_TYPE;
  successCallback: NW_CALLBACK_TYPE;
};

type NW_CALLBACK_TYPE = (response: any) => void;

export abstract class APILayer {
  public static EXECUTE(params: API_LAYER_PARAMS_TYPE) {
    const {
      apiConfig: { METHOD, URL, PARAMS },
      successCallback,
      errorCallback
    } = params;

    params.apiConfig.URL = ROOT_URL + URL;
    params.apiConfig.PARAMS = {
      ...PARAMS,
      apiKey: API_KEY
    };

    let API_PROMISE: Promise<AxiosResponse<any, any>>;
    switch (METHOD) {
      case API_METHOD.GET:
        API_PROMISE = APILayer.GET(params);
        break;
      case API_METHOD.DELETE:
        API_PROMISE = APILayer.DELETE(params);
        break;
      case API_METHOD.POST:
        API_PROMISE = APILayer.POST(params);
        break;
      case API_METHOD.PUT:
        API_PROMISE = APILayer.PUT(params);
        break;
      default:
        console.error("Method not correct. Please re-check");
        return;
    }
    API_PROMISE.then(function(response){
      if(response.status === 200){
        const { data } = response;
        successCallback(data);
      }
    }).catch(function(error){
      errorCallback(error);
    });
  }

  public static GET(params: API_LAYER_PARAMS_TYPE): Promise<AxiosResponse<any, any>>{
    const {
      apiConfig: { URL, PARAMS = {} }
    } = params;
    return axios.get(URL, {
      params: {
        ...PARAMS,
      },
      responseType: "json"
    });
  }

  public static POST(params: API_LAYER_PARAMS_TYPE) {
    const {
      apiConfig: { URL, PARAMS = {} }
    } = params;
    return axios.post(URL, {
      params: {
        ...PARAMS,
      },
    });
  }

  public static PUT(params: API_LAYER_PARAMS_TYPE) {
    const {
      apiConfig: { URL, PARAMS = {} }
    } = params;
    return axios.put(URL, {
      params: {
        ...PARAMS,
      },
    });
  }

  public static DELETE(params: API_LAYER_PARAMS_TYPE) {
    const {
      apiConfig: { URL, PARAMS = {} }
    } = params;
    return axios.delete(URL, {
      params: {
        ...PARAMS,
      },
    });
  }
}
