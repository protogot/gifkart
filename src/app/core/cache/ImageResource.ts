import { createCacheResource, CacheResource } from "./CacheResource";

const cache = new Map<string, any>();

export function loadImage(source: string): CacheResource<string>{
  let resource = cache.get(source);
  if(resource) return resource;

  resource = createCacheResource<string>(() => new Promise((resolve, reject) => {
    const img = new window.Image();
    img.src = source;
    img.addEventListener("load", function() {
      resolve(source);
    });
    img.addEventListener("error", function(err) {
      reject(source)
    });
  }));

  cache.set(source, resource);
  return resource;
}