export type CacheResource<Payload> = {
  read: () => Payload;
};

enum LoadStatus {
  pending,
  success,
  error,
}

export function createCacheResource<Payload>(
  asyncFn: () => Promise<Payload>
): CacheResource<Payload> {
  let status: LoadStatus = LoadStatus.pending,
    result: any;
  const promise = asyncFn()
    .then((payload: Payload) => {
      status = LoadStatus.success;
      result = payload;
    })
    .catch((error: Error) => {
      status = LoadStatus.error;
      result = error;
    });

  return {
    read(): Payload {
      switch (status) {
        case LoadStatus.pending:
          throw promise;
        case LoadStatus.success:
          return result;
        case LoadStatus.error:
          throw result;
      }
    },
  };
}
