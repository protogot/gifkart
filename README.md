**GIFKart - Library for GIFs and Stickers from Renowned Authors**

*The application uses www.giphy.com (Giphy) APIs to fetch the data. It is build on react, redux and hooks.*

---

## Start the project

Please follow the steps below to get the project running - 

1. Run **yarn install** to install all the node_modules.
2. Run **yarn start** to start the local dev server which supports hot reloading.
3. **Optional** - Run **yarn build** if you need the minified bundle files. It will be generated and stored in **dist** folder.
4. Your dev server would be up and running by now.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).